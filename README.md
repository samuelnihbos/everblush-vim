<div align="center">
   <img src="https://raw.githubusercontent.com/Everblush/everblush.vim/main/assets/pallette.png" alt="Everblush">
<h3> A beautiful and dark vim/neovim colorscheme.</h3>
</div>

<p align="center">
<img src="https://img.shields.io/github/stars/Everblush/everblush.vim?color=e5c76b&labelColor=22292b&style=for-the-badge"> <img src="https://img.shields.io/github/issues/Everblush/everblush.vim?color=67b0e8&labelColor=22292b&style=for-the-badge">
<img src="https://img.shields.io/static/v1?label=license&message=MIT&color=8ccf7e&labelColor=22292b&style=for-the-badge">
<img src="https://img.shields.io/github/forks/Everblush/everblush.vim?color=e74c4c&labelColor=1b2224&style=for-the-badge"> 
</p>

## Installation

# Vim-Plug
- First install <a href="https://github.com/junegunn/vim-plug">vim-plug</a>
```vimscript
Plug 'Everblush/everblush.vim'
```
- Add ```colorscheme everblush``` to .vimrc or init.vim.

# PackerNvim
```lua
use { "Everblush/everblush.vim" }`
```

# Options
For color highlight:
```vimscript
" To enable
let g:everblushNR=1 " default

" To disable
let g:everblushNR=1

" Remember to reload colorscheme after changing the variable
```

# Preview
- Bash 
<p align="center"> 
  <img src="https://raw.githubusercontent.com/Everblush/everblush.vim/main/assets/everblush-bash.png"> 
</p> 

- Lua 
<p align="center"> 
  <img src="https://raw.githubusercontent.com/Everblush/everblush.vim/main/assets/everblush-lua.png">
</p> 

# Hex Colors Codes
| Color          | Hex Code |
| :------------  | :------: |
| Foreground     | #dadada  |
| Background     | #181f21  |
| Black          | #22292b  |
| Red            | #e06e6e  |
| Green          | #8ccf7e  |
| Yellow         | #e5c76b  |
| Blue           | #67b0e8  |
| Magenta        | #c47fd5  |
| Cyan           | #6cd0ca  |
| White          | #b3b9b8  |

## Terminal Config
- <a href="https://github.com/Everblush/everblush.vim/tree/main/assets/alacritty.yml">Alacritty</a>
- <a href="https://github.com/Everblush/everblush.vim/tree/main/assets/kitty.conf">Kitty</a>
- <a href="https://github.com/Everblush/everblush.vim/tree/main/assets/.Xresources">Xresources</a>

## TO-DO
[ ] Adding statusline support.

[•] Beautifying the colors more.
